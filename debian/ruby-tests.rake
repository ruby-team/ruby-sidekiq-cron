require 'gem2deb/rake/testtask'

task :default do
  ruby = RbConfig::CONFIG['ruby_install_name']
  sh "./debian/start-redis-server.sh #{ruby} -S rake -f #{__FILE__} test"
end

Rake::TestTask.new(:test) do |t|
  t.libs << "test"
  t.test_files = FileList['test/unit/**/*_test.rb']  - FileList[
  'test/unit/launcher_test.rb']
  t.verbose = true
end
