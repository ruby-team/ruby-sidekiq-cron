#!/bin/sh

set -ex

redis-server &
PID=$!

# Stop redis-server
cleanup(){
    kill -9 $PID || true
}

trap cleanup INT EXIT TERM ALRM

"$@"
